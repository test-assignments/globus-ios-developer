//
//  ViewController.m
//  Test Assignment 1
//
//  Created by Stanislav Sidelnikov on 11/07/16.
//  Copyright © 2016 Stanislav Sidelnikov. All rights reserved.
//

#import "ViewController.h"
#import "NiceSegmentedControl.h"

@interface ViewController ()
@property (weak, nonatomic) IBOutlet NiceSegmentedControl *segmentedControl;

@end

@implementation ViewController

- (void)viewDidLoad {
  [super viewDidLoad];


  NSArray *titles = @[@"2", @"3", @"5"];
  NSMutableArray *items = [NSMutableArray array];
  for (NSString *title in titles) {
    NiceSegmentedControlItem *item = [[NiceSegmentedControlItem alloc] init];
    item.normalTitle = title;
    item.selectedTitle = [NSString stringWithFormat:@"%@ days", title];
    [items addObject:item];
  }
  [items enumerateObjectsUsingBlock:^(NiceSegmentedControlItem*  _Nonnull item, NSUInteger index, BOOL * _Nonnull stop) {
    [self.segmentedControl insertSegmentWithItem:item atIndex:index];
  }];

}
- (IBAction)segmentChanged:(NiceSegmentedControl *)sender {
  NSLog(@"Segment changed. New selected value is %ld", (long)sender.selectedSegmentIndex);
}

@end
