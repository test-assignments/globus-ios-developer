//
//  AppDelegate.h
//  Test Assignment 1
//
//  Created by Stanislav Sidelnikov on 11/07/16.
//  Copyright © 2016 Stanislav Sidelnikov. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

