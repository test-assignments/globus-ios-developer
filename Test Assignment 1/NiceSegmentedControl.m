//
//  NiceSegmentedControl.m
//  Test Assignment 1
//
//  Created by Stanislav Sidelnikov on 11/07/16.
//  Copyright © 2016 Stanislav Sidelnikov. All rights reserved.
//

#import "NiceSegmentedControl.h"

@interface NiceSegmentedControl ()

@property (nonatomic, strong) NSMutableArray *items;
@property (nonatomic, strong) NSMutableArray *labels;
@property (nonatomic, strong) NSMutableArray *linkViews;

- (void)initialize;

- (void)displayNewSelectedIndex;
- (void)layoutLabelsLinks;
- (void)setupView;
- (void)setupLabel:(UILabel *)label forItem:(NiceSegmentedControlItem *)item selected:(BOOL)selected;
- (void)setupLabels;
- (void)setupLinks;

@end

@implementation NiceSegmentedControl

#pragma mark - Lifecycle

- (void)initialize {
  _selectedColor = [UIColor colorWithRed:1.0 green:1.0 blue:1.0 alpha:1.0];
  _selectedBg = [UIColor colorWithRed:135.0/255 green:204.0/255 blue:65.0/255 alpha:1.0];
  _normalColor = [UIColor colorWithRed:135.0/255 green:204.0/255 blue:65.0/255 alpha:1.0];
  _normalBg = [UIColor colorWithRed:243.0/255 green:236.0/255 blue:230.0/255 alpha:1.0];
  _padding = 10;
  _margin = 10;
  _linkHeight = 2;

  _items = [NSMutableArray array];
  _labels = [NSMutableArray array];
  _linkViews = [NSMutableArray array];
  _selectedSegmentIndex = NiceSegmentedControlNoSegment;
  [self setupView];
}

- (instancetype)initWithFrame:(CGRect)frame {
  self = [super initWithFrame:frame];
  if (!self) return nil;

  [self initialize];

  return self;
}

- (instancetype)initWithCoder:(NSCoder *)aDecoder {
  self = [super initWithCoder:aDecoder];
  if (!self) return nil;

  [self initialize];

  return self;
}

- (void)prepareForInterfaceBuilder {
  NSArray *titles = @[@"Item 1", @"Item 2", @"Item 3"];
  NSMutableArray *items = [NSMutableArray array];
  for (NSString *title in titles) {
    NiceSegmentedControlItem *item = [[NiceSegmentedControlItem alloc] init];
    item.normalTitle = title;
    item.selectedTitle = [NSString stringWithFormat:@"%@ sel", title];
    [items addObject:item];
  }
  self.items = items;

  self.selectedSegmentIndex = 0;
}

#pragma mark - Custom Accessors

- (void)setItems:(NSMutableArray *)items {
  _items = items;
  [self setupLabels];
}

- (void)setSelectedSegmentIndex:(NSInteger)selectedSegmentIndex {
  if (self.selectedSegmentIndex != NiceSegmentedControlNoSegment) {
    UILabel *currentLabel = self.labels[self.selectedSegmentIndex];
    NiceSegmentedControlItem *item = self.items[self.selectedSegmentIndex];
    [self setupLabel:currentLabel forItem:item selected:NO];
  }
  _selectedSegmentIndex = selectedSegmentIndex;
  [self displayNewSelectedIndex];
}

#pragma mark - Public

- (void)addSegmentWithItem:(NiceSegmentedControlItem *)item {
  [self.items addObject:item];
  [self setupLabels];
}

- (void)insertSegmentWithItem:(NiceSegmentedControlItem *)item
                       atIndex:(NSUInteger)segment {
  [self.items insertObject:item atIndex:segment];
  [self setupLabels];
}

- (NiceSegmentedControlItem *)itemForSegmentAtIndex:(NSUInteger)segment {
  return self.items[segment];
}

- (NSUInteger)numberOfSegments {
  return [self.items count];
}

- (void)removeAllSegments {
  [self.items removeAllObjects];
  [self setupLabels];
}

- (void)removeSegmentAtIndex:(NSUInteger)segment {
  [self.items removeObjectAtIndex:segment];
  [self setupLabels];
}

- (void)setItem:(NiceSegmentedControlItem *)item forSegmentAtIndex:(NSUInteger)segment {
  [self.items setObject:item atIndexedSubscript:segment];
  [self setupLabels];
}

#pragma mark - Private

- (void)displayNewSelectedIndex {
  if (self.selectedSegmentIndex == NiceSegmentedControlNoSegment) {
    return;
  }
  UILabel *label = self.labels[self.selectedSegmentIndex];
  NiceSegmentedControlItem *item = self.items[self.selectedSegmentIndex];
  [self setupLabel:label forItem:item selected:YES];

  [self layoutLabelsLinks];
}

- (void)layoutLabelsLinks {
  CGRect selectedFrame = self.bounds;
  CGFloat newWidth = CGRectGetWidth(selectedFrame) / (CGFloat)[self.items count];
  selectedFrame.size.width = newWidth;

  __block CGFloat labelHeight = CGRectGetHeight(self.bounds);

  CGSize intrinsicSize = [self intrinsicContentSize];
  CGFloat totalLength = intrinsicSize.width;

  CGFloat zoomCoefficient = 1.0;
  if (totalLength > CGRectGetWidth(self.bounds)) {
    zoomCoefficient = CGRectGetWidth(self.bounds) / totalLength;
    totalLength = CGRectGetWidth(self.bounds);
  }

  __block CGFloat xPosition = CGRectGetMidX(self.bounds) - totalLength / 2;

  [self.labels enumerateObjectsUsingBlock:^(UILabel*  _Nonnull label, NSUInteger index, BOOL * _Nonnull stop) {
    CGFloat labelWidth = (label.intrinsicContentSize.width + self.padding * 2) * zoomCoefficient;
    labelWidth = MAX(labelWidth, labelHeight);
    label.frame = CGRectMake(xPosition, 0, labelWidth, labelHeight);
    label.layer.cornerRadius = CGRectGetHeight(label.frame) / 2;

    if (index > 0) {
      UIView *linkView = self.linkViews[index - 1];
      CGRect frame = linkView.frame;
      frame.size.width = self.margin * zoomCoefficient;
      frame.origin = CGPointMake(xPosition - CGRectGetWidth(frame), (labelHeight - CGRectGetHeight(frame)) / 2);
      linkView.frame = frame;
    }

    xPosition += labelWidth + self.margin;
  }];
}

- (void)setupLabels {
  for (UILabel *label in self.labels) {
    [label removeFromSuperview];
  }

  [self setupLinks];

  [self.labels removeAllObjects];

  [self.items enumerateObjectsUsingBlock:^(NiceSegmentedControlItem*  _Nonnull item, NSUInteger index, BOOL * _Nonnull stop) {
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectZero];
    [self setupLabel:label forItem:item selected:(index == self.selectedSegmentIndex)];
    [self addSubview:label];
    [self.labels addObject:label];
  }];
}

- (void)setupLabel:(UILabel *)label forItem:(NiceSegmentedControlItem *)item selected:(BOOL)selected {
  label.textAlignment = NSTextAlignmentCenter;
  if ([item.selectedTitle length]) {
    label.text = selected ? item.selectedTitle : item.normalTitle;
  } else {
    label.text = item.normalTitle;
  }
  label.backgroundColor = selected ? self.selectedBg : self.normalBg;
  label.textColor = selected ? self.selectedColor : self.normalColor;
  label.clipsToBounds = YES;
  // If we need to adjust the font, it should be scaled equally in all labels. Don't need to do that now
  label.adjustsFontSizeToFitWidth = NO;
}

- (void)setupLinks {
  for (UIView *linkView in self.linkViews) {
    [linkView removeFromSuperview];
  }

  [self.linkViews removeAllObjects];

  CGRect linkFrame = CGRectMake(0, 0, self.margin, self.linkHeight);
  for (NSInteger i = 0; i < [self.items count] - 1; i++) {
    UIView *linkView = [[UIView alloc] initWithFrame:linkFrame];
    linkView.backgroundColor = self.normalBg;
    [self.linkViews addObject:linkView];
    [self addSubview:linkView];
  }
}

- (void)setupView {
  self.backgroundColor = [UIColor clearColor];
}

#pragma mark - Superclass Overrides

- (BOOL)beginTrackingWithTouch:(UITouch *)touch withEvent:(UIEvent *)event {
  CGPoint location = [touch locationInView:self];

  [self.labels enumerateObjectsUsingBlock:^(UILabel*  _Nonnull label, NSUInteger index, BOOL * _Nonnull stop) {
    if (CGRectContainsPoint(label.frame, location)) {
      self.selectedSegmentIndex = index;
      [self sendActionsForControlEvents:UIControlEventValueChanged];
      *stop = YES;
    }
  }];

  return NO;
}

- (CGSize)intrinsicContentSize {
  CGFloat totalLength = 0;
  for (UILabel *label in self.labels) {
    totalLength += CGRectGetWidth(label.frame);
  }
  if ([self.items count] > 1) {
    totalLength += (CGFloat)([self.items count] - 1) * self.margin;
  }

  return CGSizeMake(totalLength, UIViewNoIntrinsicMetric);
}

- (void)layoutSubviews {
  [super layoutSubviews];

  [self layoutLabelsLinks];
  [self invalidateIntrinsicContentSize];
}

@end

@implementation NiceSegmentedControlItem
@end