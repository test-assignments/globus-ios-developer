//
//  NiceSegmentedControl.h
//  Test Assignment 1
//
//  Created by Stanislav Sidelnikov on 11/07/16.
//  Copyright © 2016 Stanislav Sidelnikov. All rights reserved.
//

#import <UIKit/UIKit.h>

enum {
  NiceSegmentedControlNoSegment = -1   // segment index for no selected segment
};

@interface NiceSegmentedControlItem : NSObject
@property(nonatomic, strong) NSString *normalTitle;
@property(nonatomic, strong) NSString *selectedTitle;
@end

IB_DESIGNABLE
@interface NiceSegmentedControl : UIControl

- (void)addSegmentWithItem:(NiceSegmentedControlItem *)item;
- (void)insertSegmentWithItem:(NiceSegmentedControlItem *)item
                      atIndex:(NSUInteger)segment;
- (NiceSegmentedControlItem *)itemForSegmentAtIndex:(NSUInteger)segment;
- (NSUInteger)numberOfSegments;
- (void)removeAllSegments;
- (void)removeSegmentAtIndex:(NSUInteger)segment;
- (void)setItem:(NiceSegmentedControlItem *)item forSegmentAtIndex:(NSUInteger)segment;

@property(nonatomic) NSInteger selectedSegmentIndex;

@property(nonatomic, strong) IBInspectable UIColor *selectedColor;
@property(nonatomic, strong) IBInspectable UIColor *selectedBg;
@property(nonatomic, strong) IBInspectable UIColor *normalColor;
@property(nonatomic, strong) IBInspectable UIColor *normalBg;
@property(nonatomic) IBInspectable NSInteger padding;
@property(nonatomic) IBInspectable NSInteger margin;
@property(nonatomic) IBInspectable NSInteger linkHeight;

@end
