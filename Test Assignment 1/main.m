//
//  main.m
//  Test Assignment 1
//
//  Created by Stanislav Sidelnikov on 11/07/16.
//  Copyright © 2016 Stanislav Sidelnikov. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
  @autoreleasepool {
      return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
  }
}
