## Useful links:

* [Ray Wenderlich's Objective-C Style Guide](https://github.com/raywenderlich/objective-c-style-guide).
* [Customizing UISegmentedControl](http://smnh.me/customizing-appearance-of-uisegmentedcontrol/).
* [Custom UISegmentedControl with IBDesignable in Swift](http://www.appdesignvault.com/custom-segmented-control-swift-tutorial/).
* [Custom Segmented Control in Objective-C](https://github.com/alikaragoz/AKSegmentedControl).